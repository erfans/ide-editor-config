We can use editor configurations to format the code in a common style in order to keep a similar code style between developers. 
It will help to:

- make the code more understandable  
- reduce the redundant diffs for git due to code formatting  


### Standardization

In many cases there is no standard style for a language, however to keep it similar with the community 
we should try to follow the language conventions

**Here you can find a list of conventions for languages (you can add the missing ones)**

- [css](https://cssguidelin.es/#syntax-and-formatting)
- [sass](https://sass-guidelin.es/#syntax--formatting)
- [yaml](https://docs.saltstack.com/en/latest/topics/troubleshooting/yaml_idiosyncrasies.html#indentation)
  

### Configuration sharing
This repository contains two types of configurations for different IDEs.

####.editorconfig
.editorconfig supports majority of editors (natively or through plugins). The further information can be found in the official website. You can download the .editorconfig file from this repository and include it to your project.
To read more about .editorconfig visit [editorconfig.org](https://editorconfig.org/).

#### PHPStorm 

PHPStorm supports more advanced configurations. The repository contains `settings.zip` file which can be downloaded and imported to the PHPStorm.

**-- Important note --**  
**importing this settings will override some of the PHPStorm configurations (Code styles and File templates). Please first make a backup from your configurations.**  

[Here](https://www.jetbrains.com/help/phpstorm/exporting-and-importing-settings.html) you can find out how to import or export configurations to/from PHPStrom.  
Two types of configuration exist in the settings, code styles and file templates.

###### FILE TEMPLATES

To use the file templates you need to change the name of current user in php storm variables. 
PhpStorm by default uses the username of the current user. To change it to the full name from Help > Edit Custom VM Options..., add the following line at the end of opened window.

```
-Duser.name=Your Fullname
```

The final file should be similar to this.

```
-Xms128m
-Xmx2000m
-XX:ReservedCodeCacheSize=240m
-XX:+UseCompressedOops
-Dfile.encoding=UTF-8
-XX:+UseConcMarkSweepGC
-XX:SoftRefLRUPolicyMSPerMB=50
-ea
-Dsun.io.useCanonCaches=false
-Djava.net.preferIPv4Stack=true
-Djdk.http.auth.tunneling.disabledSchemes=""
-XX:+HeapDumpOnOutOfMemoryError
-XX:-OmitStackTraceInFastThrow
-Xverify:none

-XX:ErrorFile=$USER_HOME/java_error_in_phpstorm_%p.log
-XX:HeapDumpPath=$USER_HOME/java_error_in_phpstorm.hprof
-Dide.no.platform.update=true
-Duser.name=Your Fullname
```

### Improvement/Contribution

If the configuration does not match your requirements or you want to add a language specific configuration, 
after you made your changes to your local copy, please also make a pull request or send a copy to me. 


[Here](https://www.jetbrains.com/help/phpstorm/exporting-and-importing-settings.html) you can find out how to import or export configurations to/from PHPStrom.

**-- Important note --**  
**When you export the configuration from  PHPStorm for contribution just check the Code styles and File templates.**
